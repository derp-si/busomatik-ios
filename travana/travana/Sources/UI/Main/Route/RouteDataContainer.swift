//
//  RouteContainer.swift
//  travana
//
//  Created by Domen Kralj on 17/10/2020.
//  Copyright © 2020 Domen Kralj. All rights reserved.
//

import Foundation
import CoreLocation

class RouteDataContainer {

    public var routeStationArrivals: [LppCompatibleStationArrival]
    public var busesOnRoute: [LppCompatibleBus]
    public var route: LppCompatibleRoute?
    
    init (routeStationArrivals: [LppCompatibleStationArrival], busesOnRoute: [LppCompatibleBus], route: LppCompatibleRoute?) {
        self.routeStationArrivals = routeStationArrivals
        self.busesOnRoute = busesOnRoute
        self.route = route
    }
}
