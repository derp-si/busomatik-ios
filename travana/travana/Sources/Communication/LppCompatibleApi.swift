//
//  LppApi.swift
//  travana
//
//  Created by Domen Kralj on 28/09/2020.
//  Copyright © 2020 Domen Kralj. All rights reserved.
//

import Foundation

import FeedKit

// client for retirving data from https://data.lpp.si/doc
// class used for operating with lpp api backend (data)
class LppCompatibleApi {
    private static let API_BASE = "https://marprom-lpp.ojpp.derp.si/api"
    
    private static let LPP_DETOURS_INFO = "https://www.marprom.si/feed/"
    
    private static let ACTIVE_ROUTES_LINK = "\(API_BASE)/route/active-routes"
    private static let ROUTES_LINK = "\(API_BASE)/route/routes"
    private static let STATIONS_DETAILS_LINK = "\(API_BASE)/station/station-details"
    private static let ARRIVALS_ON_ROUTE_LINK = "\(API_BASE)/route/arrivals-on-route"
    private static let BUSES_ON_ROUTE_LINK = "\(API_BASE)/bus/buses-on-route"
    private static let ROUTES_ON_STATION_LINK = "\(API_BASE)/station/routes-on-station"
    private static let TIMETABLE_LINK = "\(API_BASE)/station/timetable"
    private static let ARRIVAL_LINK = "\(API_BASE)/station/arrival"
    
    private static let preferences = UserDefaults.standard
    private static let IS_IN_FAVORITES_KEY = "IS_IN_FAVORITES_KEY_"
    
    private let logger: ConsoleLogger = LoggerFactory.getLogger(name: "LppCompatibleApi")
    private let httpClient: HttpClient
    private let decoder: JSONDecoder
    
    // used for caching data - once data is loaded do not retrive it again
    private var stations: [LppCompatibleStation]? = nil
    private var routes: [LppCompatibleRoute]? = nil
    
    
    required init?(httpClient: HttpClient) {
        self.httpClient = httpClient
        self.decoder = JSONDecoder()
    }
    
    public static func addStationToFavorites(stationRefId: String) {
        let key = LppCompatibleApi.IS_IN_FAVORITES_KEY + stationRefId
        LppCompatibleApi.preferences.set(true, forKey: key)
        preferences.synchronize()
    }
    
    public static func removeStationFromFavorites(stationRefId: String) {
        let key = LppCompatibleApi.IS_IN_FAVORITES_KEY + stationRefId
        LppCompatibleApi.preferences.set(false, forKey: key)
        preferences.synchronize()
    }
    
    public static func isStationInFavorites(stationRefId: String) -> Bool {
        let key = LppCompatibleApi.IS_IN_FAVORITES_KEY + stationRefId
        
        if LppCompatibleApi.preferences.object(forKey: key) == nil {
            return false
        } else {
            return LppCompatibleApi.preferences.bool(forKey: key)
        }
    }
    
    public func getStations(callback: @escaping (Response<[LppCompatibleStation]>) -> ()) {
        
        // if data was already loaded - return cached stations
        if self.stations != nil {
            callback(Response.success(data: self.stations!))
            return
        }
        
        let params = ["show-subroutes": "1"]
        
        var stations: [LppCompatibleStation]? = nil
        
        self.httpClient.getRequest(urlStr: LppCompatibleApi.STATIONS_DETAILS_LINK, params: params, headers: nil) { [weak self] (error, response) in
            guard let self = self else { return }
            
            if error != nil {
                let errorMessage = "Error retrieving stations data, error: \(error!)"
                self.logger.error(errorMessage)
                let error = RequestError.RequestFailedException(errorMessage)
                callback(Response.failure(error: error))
                return
            }
            
            if response == nil {
                let errorMessage = "Error retrieving stations data, response is null"
                self.logger.error(errorMessage)
                let error = RequestError.RequestFailedException(errorMessage)
                callback(Response.failure(error: error))
                return
            }
            do {
                stations = try self.decoder.decode(LppCompatibleApiResponse<[LppCompatibleStation]>.self, from: Data(response!.utf8)).data
                self.stations = stations
                callback(Response.success(data: stations!))
            } catch let parseError {
                let errorMessage = "Error retrieving stations data, parsing json to object failed: \(parseError)"
                self.logger.error(errorMessage)
                let error = RequestError.RequestFailedException(errorMessage)
                callback(Response.failure(error: error))
                return
            }
        }
    }
    
    // returns route data with route path
    public func getDetailedRoute(routeId: String, callback: @escaping (Response<[LppCompatibleRoute]>) -> ())  {
        
        let params = ["route_id": routeId,
                      "shape": "1"]
        
        var routes: [LppCompatibleRoute]? = nil
        
        self.httpClient.getRequest(urlStr: LppCompatibleApi.ROUTES_LINK, params: params, headers: nil) { [weak self] (error, response) in
            guard let self = self else { return }
            
            if error != nil {
                let errorMessage = "Error retrieving routes data, error: \(error!)"
                self.logger.error(errorMessage)
                let error = RequestError.RequestFailedException(errorMessage)
                callback(Response.failure(error: error))
                return
            }
            
            if response == nil {
                let errorMessage = "Error retrieving routes data, response is null"
                self.logger.error(errorMessage)
                let error = RequestError.RequestFailedException(errorMessage)
                callback(Response.failure(error: error))
                return
            }
            do {
                routes = try self.decoder.decode(LppCompatibleApiResponse<[LppCompatibleRoute]>.self, from: Data(response!.utf8)).data
                callback(Response.success(data: routes!))
            } catch let parseError {
                let errorMessage = "Error retrieving routes data, parsing json to object failed: \(parseError)"
                self.logger.error(errorMessage)
                let error = RequestError.RequestFailedException(errorMessage)
                callback(Response.failure(error: error))
                return
            }
        }
    }
    
    public func getRoutes(callback: @escaping (Response<[LppCompatibleRoute]>) -> ()) {
        
        if self.routes != nil {
            callback(Response.success(data: self.routes!))
            return
        }
        
        var routes: [LppCompatibleRoute]? = nil
        
        self.httpClient.getRequest(urlStr: LppCompatibleApi.ACTIVE_ROUTES_LINK, params: nil, headers: nil) { [weak self] (error, response) in
            guard let self = self else { return }
            
            if error != nil {
                let errorMessage = "Error retrieving routes data, error: \(error!)"
                self.logger.error(errorMessage)
                let error = RequestError.RequestFailedException(errorMessage)
                callback(Response.failure(error: error))
                return
            }
            
            if response == nil {
                let errorMessage = "Error retrieving routes data, response is null"
                self.logger.error(errorMessage)
                let error = RequestError.RequestFailedException(errorMessage)
                callback(Response.failure(error: error))
                return
            }
            do {
                routes = try self.decoder.decode(LppCompatibleApiResponse<[LppCompatibleRoute]>.self, from: Data(response!.utf8)).data
                self.routes = routes
                callback(Response.success(data: routes!))
            } catch let parseError {
                let errorMessage = "Error retrieving routes data, parsing json to object failed: \(parseError)"
                self.logger.error(errorMessage)
                let error = RequestError.RequestFailedException(errorMessage)
                callback(Response.failure(error: error))
                return
            }
        }
    }
    
    public func getStationsAndBusRoutes(callback: @escaping (Response<[String: Any]>) -> ()) {
        
        // the callback in the function paramater is called with success when both request are successfull
        // weather day data and weather week data
        var isOneRequestSuccessful = false
        var routes: [LppCompatibleRoute]? = nil
        var stations: [LppCompatibleStation]? = nil
        
        self.getStations() { (result) in
            if result.success {
                stations = result.data!
                if isOneRequestSuccessful {
                    let routesAndStationsData = ["routes": routes!, "stations": stations!] as [String : Any]
                    callback(Response.success(data: routesAndStationsData))
                    return
                } else {
                    isOneRequestSuccessful = true
                }
            } else {
                let errorMessage = "Error retrieving arrivals on route data: \(result.error!)"
                let error = RequestError.RequestFailedException(errorMessage)
                callback(Response.failure(error: error))
                return
            }
        }
        
        self.getRoutes() {(result) in
            if result.success {
                routes = result.data!
                if isOneRequestSuccessful {
                    let routesAndStationsData = ["routes": routes!, "stations": stations!] as [String : Any]
                    callback(Response.success(data: routesAndStationsData))
                    return
                } else {
                    isOneRequestSuccessful = true
                }
            } else {
                let errorMessage = "Error retrieving buses on route data: \(result.error!)"
                let error = RequestError.RequestFailedException(errorMessage)
                callback(Response.failure(error: error))
                return
            }
        }
    }
    
    public func getArrivalsOnRoute(tripId: String, callback: @escaping (Response<[LppCompatibleStationArrival]>) -> ()) {
        
        let params = ["trip-id": tripId]
        
        var stationsArrivals: [LppCompatibleStationArrival]? = nil
        
        self.httpClient.getRequest(urlStr: LppCompatibleApi.ARRIVALS_ON_ROUTE_LINK, params: params, headers: nil) { [weak self] (error, response) in
            guard let self = self else { return }
            
            if error != nil {
                let errorMessage = "Error retrieving arrivals on route data, error: \(error!)"
                self.logger.error(errorMessage)
                let error = RequestError.RequestFailedException(errorMessage)
                callback(Response.failure(error: error))
                return
            }
            
            if response == nil {
                let errorMessage = "Error retrieving arrivals on route data, response is null"
                self.logger.error(errorMessage)
                let error = RequestError.RequestFailedException(errorMessage)
                callback(Response.failure(error: error))
                return
            }
            
            do {
                stationsArrivals = try self.decoder.decode(LppCompatibleApiResponse<[LppCompatibleStationArrival]>.self, from: Data(response!.utf8)).data!
                callback(Response.success(data: stationsArrivals!))
            } catch let parseError {
                let errorMessage = "Error retrieving arrivals on route data, parsing json to object failed: \(parseError)"
                self.logger.error(errorMessage)
                let error = RequestError.RequestFailedException(errorMessage)
                callback(Response.failure(error: error))
                return
            }
        }
    }
    
    public func getBusesOnRoute(routeGroupNumber: String, callback: @escaping (Response<[LppCompatibleBus]>) -> ()) {
        
        let params = ["route-group-number": routeGroupNumber, "specific": "1"]
        
        let headers = ["apikey": "D2F0C381-6072-45F9-A05E-513F1515DD6A"]
        
        var busesOnRoute: [LppCompatibleBus]? = nil
        
        self.httpClient.getRequest(urlStr: LppCompatibleApi.BUSES_ON_ROUTE_LINK, params: params, headers: headers) { [weak self] (error, response) in
            guard let self = self else { return }
            
            if error != nil {
                let errorMessage = "Error retrieving buses on route data, error: \(error!)"
                self.logger.error(errorMessage)
                let error = RequestError.RequestFailedException(errorMessage)
                callback(Response.failure(error: error))
                return
            }
            
            if response == nil {
                let errorMessage = "Error retrieving buses on route data, response is null"
                self.logger.error(errorMessage)
                let error = RequestError.RequestFailedException(errorMessage)
                callback(Response.failure(error: error))
                return
            }
            
            do {
                busesOnRoute = try self.decoder.decode(LppCompatibleApiResponse<[LppCompatibleBus]>.self, from: Data(response!.utf8)).data!
                callback(Response.success(data: busesOnRoute!))
            } catch let parseError {
                let errorMessage = "Error retrieving buses on route data, parsing json to object failed: \(parseError)"
                self.logger.error(errorMessage)
                let error = RequestError.RequestFailedException(errorMessage)
                callback(Response.failure(error: error))
                return
            }
        }
    }
    
    public func getRoutesOnStation(stationCode: String, callback: @escaping (Response<[LppCompatibleRouteOnStation]>) -> ()) {
        
        let params = ["station-code": stationCode]
        
        var routesOnStation: [LppCompatibleRouteOnStation]? = nil
        
        self.httpClient.getRequest(urlStr: LppCompatibleApi.ROUTES_ON_STATION_LINK, params: params, headers: nil) { [weak self] (error, response) in
            guard let self = self else { return }
            
            if error != nil {
                let errorMessage = "Error retrieving routes on station data, error: \(error!)"
                self.logger.error(errorMessage)
                let error = RequestError.RequestFailedException(errorMessage)
                callback(Response.failure(error: error))
                return
            }
            
            if response == nil {
                let errorMessage = "Error retrieving routes on station data, response is null"
                self.logger.error(errorMessage)
                let error = RequestError.RequestFailedException(errorMessage)
                callback(Response.failure(error: error))
                return
            }
            
            do {
                routesOnStation = try self.decoder.decode(LppCompatibleApiResponse<[LppCompatibleRouteOnStation]>.self, from: Data(response!.utf8)).data!
                callback(Response.success(data: routesOnStation!))
            } catch let parseError {
                let errorMessage = "Error retrieving routes on station data, parsing json to object failed: \(parseError)"
                self.logger.error(errorMessage)
                let error = RequestError.RequestFailedException(errorMessage)
                callback(Response.failure(error: error))
                return
            }
        }
    }
    
    public func getTimetable(stationCode: String, routeGroupNumber: String, nextHours: Int, previousHours: Int, callback: @escaping (Response<LppTimetable>) -> ()) {
        
        let params = ["station-code": stationCode, "route-group-number": routeGroupNumber, "next-hours" : String(nextHours), "previous-hours" : String(previousHours)]
        
        var timetable: LppTimetable? = nil
        
        self.httpClient.getRequest(urlStr: LppCompatibleApi.TIMETABLE_LINK, params: params, headers: nil) { [weak self] (error, response) in
            guard let self = self else { return }
            
            if error != nil {
                let errorMessage = "Error retrieving timetable data, error: \(error!)"
                self.logger.error(errorMessage)
                let error = RequestError.RequestFailedException(errorMessage)
                callback(Response.failure(error: error))
                return
            }
            
            if response == nil {
                let errorMessage = "Error retrieving timetable data, response is null"
                self.logger.error(errorMessage)
                let error = RequestError.RequestFailedException(errorMessage)
                callback(Response.failure(error: error))
                return
            }
            
            do {
                timetable = try self.decoder.decode(LppCompatibleApiResponse<LppTimetable>.self, from: Data(response!.utf8)).data!
                callback(Response.success(data: timetable!))
            } catch let parseError {
                let errorMessage = "Error retrieving timetable data, parsing json to object failed: \(parseError)"
                self.logger.error(errorMessage)
                let error = RequestError.RequestFailedException(errorMessage)
                callback(Response.failure(error: error))
                return
            }
        }
    }
    
    public func getArrivals(stationCode: String, callback: @escaping (Response<[LppCompatibleArrival2]>) -> ()) {
        
        let params = ["station-code": stationCode]
        
        var arrivals: [LppCompatibleArrival2]? = nil
        
        self.httpClient.getRequest(urlStr: LppCompatibleApi.ARRIVAL_LINK, params: params, headers: nil) { [weak self] (error, response) in
            guard let self = self else { return }
            
            if error != nil {
                let errorMessage = "Error retrieving arrival data, error: \(error!)"
                self.logger.error(errorMessage)
                let error = RequestError.RequestFailedException(errorMessage)
                callback(Response.failure(error: error))
                return
            }
            
            if response == nil {
                let errorMessage = "Error retrieving arrival data, response is null"
                self.logger.error(errorMessage)
                let error = RequestError.RequestFailedException(errorMessage)
                callback(Response.failure(error: error))
                return
            }
            
            do {
                arrivals = try self.decoder.decode(LppCompatibleApiResponse<LppCompatibleArrival2Wrapper>.self, from: Data(response!.utf8)).data!.arrivals
                callback(Response.success(data: arrivals!))
            } catch let parseError {
                let errorMessage = "Error retrieving arrival data, parsing json to object failed: \(parseError)"
                self.logger.error(errorMessage)
                let error = RequestError.RequestFailedException(errorMessage)
                callback(Response.failure(error: error))
                return
            }
        }
    }
    
    public func getBusesAndArrivalsOnRoute(tripId: String, routeGroupNumber: String, callback: @escaping (Response<RouteDataContainer>) -> ()) {
        
        var numOfSuccesfulRequests = 0
        var routeStationArrivals: [LppCompatibleStationArrival]? = nil
        var busesOnRoute: [LppCompatibleBus]? = nil
        
        self.getArrivalsOnRoute(tripId: tripId) {
            (result) in
            if result.success {
                routeStationArrivals = result.data!
                if numOfSuccesfulRequests == 1 {
                    let routeData = RouteDataContainer(routeStationArrivals: routeStationArrivals!, busesOnRoute: busesOnRoute!, route: nil)
                    callback(Response.success(data: routeData))
                } else {
                    numOfSuccesfulRequests += 1
                }
            } else {
                let errorMessage = "Error retrieving arrivals on route data: \(result.error!)"
                let error = RequestError.RequestFailedException(errorMessage)
                callback(Response.failure(error: error))
                return
            }
        }
        
        self.getBusesOnRoute(routeGroupNumber: routeGroupNumber) {
            (result) in
            if result.success {
                busesOnRoute = result.data!
                if numOfSuccesfulRequests == 1 {
                    let routeData = RouteDataContainer(routeStationArrivals: routeStationArrivals!, busesOnRoute: busesOnRoute!, route: nil)
                    callback(Response.success(data: routeData))
                } else {
                    numOfSuccesfulRequests += 1
                }
            } else {
                let errorMessage = "Error retrieving buses on route data: \(result.error!)"
                let error = RequestError.RequestFailedException(errorMessage)
                callback(Response.failure(error: error))
                return
            }
        }
        
    }
    
    // returns busses on route, arrivals for the route and detailed route path
    public func getFullDetailedRouteData(tripId: String, routeId: String, routeGroupNumber: String, callback: @escaping (Response<RouteDataContainer>) -> ()) {
        
        var numOfSuccesfulRequests = 0
        var routeStationArrivals: [LppCompatibleStationArrival]? = nil
        var busesOnRoute: [LppCompatibleBus]? = nil
        var route: LppCompatibleRoute? = nil
        
        self.getArrivalsOnRoute(tripId: tripId) {
            (result) in
            if result.success {
                routeStationArrivals = result.data!
                if numOfSuccesfulRequests == 2 {
                    let routeData = RouteDataContainer(routeStationArrivals: routeStationArrivals!, busesOnRoute: busesOnRoute!, route: route)
                    callback(Response.success(data: routeData))
                } else {
                    numOfSuccesfulRequests += 1
                }
            } else {
                let errorMessage = "Error retrieving arrivals on route data: \(result.error!)"
                let error = RequestError.RequestFailedException(errorMessage)
                callback(Response.failure(error: error))
                return
            }
        }
        
        self.getBusesOnRoute(routeGroupNumber: routeGroupNumber) {
            (result) in
            if result.success {
                busesOnRoute = result.data!
                if numOfSuccesfulRequests == 2 {
                    let routeData = RouteDataContainer(routeStationArrivals: routeStationArrivals!, busesOnRoute: busesOnRoute!, route: route)
                    callback(Response.success(data: routeData))
                } else {
                    numOfSuccesfulRequests += 1
                }
            } else {
                let errorMessage = "Error retrieving buses on route data: \(result.error!)"
                let error = RequestError.RequestFailedException(errorMessage)
                callback(Response.failure(error: error))
                return
            }
        }
        
        self.getDetailedRoute(routeId: routeId) {
            (result) in
            
            numOfSuccesfulRequests += 1
            if result.success {
                for routeIter in result.data! {
                    if routeIter.tripId == tripId {
                        route = routeIter
                    }
                }
            }
            if numOfSuccesfulRequests == 3 {
                let routeData = RouteDataContainer(routeStationArrivals: routeStationArrivals!, busesOnRoute: busesOnRoute!, route: route)
                callback(Response.success(data: routeData))
                return
            }
        }
    }
    
    public func getDetoursInfo(callback: @escaping (Response<[LppCompatibleDetourInfo]>) -> ()) {
        
        var detours: [LppCompatibleDetourInfo]? = nil
        
        self.httpClient.getRequest(urlStr: LppCompatibleApi.LPP_DETOURS_INFO, params: nil, headers: nil) { [weak self] (error, response) in
            guard let self = self else { return }
            
            if error != nil {
                let errorMessage = "Error retrieving detours info, error: \(error!)"
                self.logger.error(errorMessage)
                let error = RequestError.RequestFailedException(errorMessage)
                callback(Response.failure(error: error))
                return
            }
            
            if response == nil {
                let errorMessage = "Error retrieving detours info, response is null"
                self.logger.error(errorMessage)
                let error = RequestError.RequestFailedException(errorMessage)
                callback(Response.failure(error: error))
                return
            }
            
            do {
                detours = try self.getDetoursFromRss(rssString: response!)
                if detours == nil {
                    let errorMessage = "Error retrieving detours info, cannot extract detours from rss string"
                    self.logger.error(errorMessage)
                    let error = RequestError.RequestFailedException(errorMessage)
                    callback(Response.failure(error: error))
                    return
                }
                callback(Response.success(data: detours!))
            } catch let Error {
                let errorMessage = "Error retrieving detours info parsing json to object failed: \(Error)"
                self.logger.error(errorMessage)
                let error = RequestError.RequestFailedException(errorMessage)
                callback(Response.failure(error: error))
                return
            }
        }
    }
    
    public func getDetoursFromRss(rssString: String) throws -> [LppCompatibleDetourInfo]? {
        
        let parser = FeedParser(data: rssString.data(using: .utf8)!)
        
        var detours: [LppCompatibleDetourInfo] = []
        
        let parseResult = parser.parse()
        
        switch parseResult {
        case .success(let feed): do {
                
                // Grab the parsed feed directly as an optional rss, atom or json feed object
            feed.rssFeed?.items?.forEach({ RSSFeedItem in
                if (!(RSSFeedItem.title?.contains(find: "Obvoz") ?? false)) {return;}
                
                detours.append(LppCompatibleDetourInfo(title: RSSFeedItem.title ?? "", date: DateFormatter().string(from: RSSFeedItem.pubDate!), moreDataUrl: RSSFeedItem.link ?? ""))
            })
            }
                
            case .failure(let error):
                print(error)
        }
        
        return detours
    }
    
}
